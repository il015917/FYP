import open3d as o3d
import numpy as np
import os
import nibabel as nib
from argparse import Namespace
from tkinter import filedialog


def get_options():
    while True:
        i = input('Path to the file: ').strip()
        if os.path.exists(i):
            break
        print("could not find the file!")

    try:
        s = int(input('Scale (default 8): ').strip())
    except:
        print("Going to use scale 8")
        s = 8

    try:
        t = float(input('Windowsize (default 0.1): ').strip())
    except:
        print("Going to use scale 0.1")
        t = 0.1

    nsp = Namespace(input=i, scale=s, threshold=t)
    return nsp


def load_nifti_img(filepath, dtype):
    '''
    NIFTI Image Loader
    :param filepath: path to the input NIFTI image
    :param dtype: dataio type of the nifti numpy array
    :return: return numpy array
    '''
    nim = nib.load(filepath)
    out_nii_array = np.array(nim.get_fdata(), dtype=dtype)
    out_nii_array = np.squeeze(out_nii_array)  # drop singleton dim in case temporal dim exists
    meta = {'affine': nim.affine,
            'dim': nim.header['dim'],
            'pixdim': nim.header['pixdim'],
            'name': os.path.basename(filepath)
            }

    return out_nii_array, meta


def display_data(vision_data):
    scaled = vision_data[::opts.scale, ::opts.scale, ::opts.scale]
    m = scaled.max()

    points = []
    colors = []

    for y, slicing in enumerate(scaled):
        for x, row in enumerate(slicing):
            for z, val in enumerate(row):
                if val / m < opts.threshold:
                    continue
                points.append([x, y, z])
                colors.append([val / m, val / m, val / m])

    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(np.array(points))
    pcd.colors = o3d.utility.Vector3dVector(np.array(colors))
    o3d.visualization.draw_geometries([pcd])


if __name__ == '__main__':
    opts = get_options()
    data = load_nifti_img(opts.input, np.int16)[0]
    display_data(data)
