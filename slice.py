import numpy as np
import os
import nibabel as nib
from argparse import Namespace
from tkinter import *

def get_options():
    while True:
        i = input('Path to the file: ').strip()
        if os.path.exists(i):
            break
        print("could not find the file!")

    try:
        s = int(input('Scale (default 2): ').strip())
    except:
        print("Going to use scale 2")
        s = 2

    try:
        ws = int(input('Windowsize (default 512): ').strip())
    except:
        print("Going to use scale 512")
        ws = 512

    nsp = Namespace(input=i, scale=s, windowSize=ws)

    return nsp

def load_nifti_img(filepath, dtype):
    '''
    NIFTI Image Loader
    :param filepath: path to the input NIFTI image
    :param dtype: dataio type of the nifti numpy array
    :return: return numpy array
    '''
    nim = nib.load(filepath)
    out_nii_array = np.array(nim.get_fdata(), dtype=dtype)
    out_nii_array = np.squeeze(out_nii_array)  # drop singleton dim in case temporal dim exists
    meta = {'affine': nim.affine,
            'dim': nim.header['dim'],
            'pixdim': nim.header['pixdim'],
            'name': os.path.basename(filepath)
            }

    return out_nii_array, meta


def draw():
    c.delete('all')
    m = data[:, :, level].max()
    for y, row in enumerate(data[:, :, level]):
        for x, val in enumerate(row):
            colVal = 256 - int(val / m * 256)
            rgb = (hex(colVal)[2:]) * 3
            c.create_rectangle(x * opts.scale, y * opts.scale, x * opts.scale + opts.scale, y * opts.scale + opts.scale,
                               fill=f'#{rgb}', outline=f'#{rgb}', tags="pixel")
    c.create_text(64, 16, text=f'layer {level + 1} of {max_level + 1}', fill='red')


def move(e):
    global last_x, last_y, total_d_x, total_d_y
    d_x = e.x - last_x
    d_y = e.y - last_y
    last_x = e.x
    last_y = e.y
    total_d_x += d_x
    total_d_y += d_y
    c.move('pixel', d_x, d_y)

def scroll(e):
    global level
    level += e.delta
    level = min(level, max_level)
    level = max(0, level)
    draw()
    c.move('pixel', total_d_x, total_d_y)

def click(e):
    global last_x, last_y
    last_x = e.x
    last_y = e.y

if __name__ == '__main__':
    opts = get_options()
    data = load_nifti_img(opts.input, np.int16)[0]

    window = Tk()
    c = Canvas(window, width=opts.windowSize, height=opts.windowSize)
    c.pack()

    c.bind("<B1-Motion>", move)
    c.bind("<MouseWheel>", scroll)
    c.bind('<Button-1>', click)

    level = 100
    total_d_x = total_d_y = 0
    last_x = last_y = 0
    max_level = data.shape[2] - 1

    draw()

    window.mainloop()