import os
import tkinter as tk
from tkinter import filedialog

import numpy as np
import scipy.ndimage as ndi
import nibabel as nib
from nibabel.viewers import OrthoSlicer3D

import matplotlib.pyplot as plt

# Create gui
window = tk.Tk()
window.title("FYP")
window.config(background='#e3e3e3')
window.geometry("200x300")

# Create label
l = tk.Label(window, text="MRI Visualizer", background="#e3e3e3")
l.config(font=("Square Sans Serif", 14))

# Dummy Labels
l2 = tk.Label(window, text="", background="#e3e3e3")
l3 = tk.Label(window, text="", background="#e3e3e3")

# To load and display the NifTi files in three sections
def view():
    view = filedialog.askopenfilename()
    print(view)

    # Load NifTi file
    img_obj = nib.load(view)
    print(f'Type of the image {type(img_obj)}.')

    # Get image data
    image_data = img_obj.get_fdata()
    type(image_data)

    # Display the NifTi image in three parts.
    OrthoSlicer3D(image_data, title='Brain').show()

    return view

# Slice the image into multiple pictures of the brain
def slice():
    slice = filedialog.askopenfilename()
    print(slice)

    # Load NifTi file
    img_obj = nib.load(slice)
    print(f'Type of the image {type(img_obj)}.')

    # Get image data
    image_data = img_obj.get_fdata()
    type(image_data)

    # Data Slicing
    fig_rows = 4
    fig_cols = 4
    n_subplots = fig_rows * fig_cols
    n_slice = image_data.shape[0]
    step_size = n_slice // n_subplots
    plot_range = n_subplots * step_size
    start_stop = int((n_slice - plot_range) / 2)

    fig, axs = plt.subplots(fig_rows, fig_cols, figsize=[10, 10])

    for idx, img in enumerate(range(start_stop, plot_range, step_size)):
        axs.flat[idx].imshow(ndi.rotate(image_data[img, :, :], 90), cmap='gray')
        axs.flat[idx].axis('off')

    plt.tight_layout()
    plt.show()


# Buttons
b1 = tk.Button(window, text='NifTi Viewer', command=view)

b2 = tk.Button(window, text='NifTi Slicer', command=slice)

l.pack()
l2.pack()
b1.pack()
l3.pack()
b2.pack()

window.mainloop()
